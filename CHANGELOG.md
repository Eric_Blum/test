Changelog
=========

Unreleased
----------

### Internal


##### New Features

* Created ./deploy.sh script to manage pulling, pip install, migrations,
  collectstatic, and restarting uwsgi and supervisor in one easy step.
  [Eric Blum]


##### Changes

* Split change log by commit type (fix/change/new) under wider
  categorization of Client Facing and Internal. [Eric Blum]

* Change template language from mustache to mako for more flexibility.
  [Eric Blum]

* Add -r flag to deploy.sh to only restart uwsgi and supervisor. [Eric
  Blum]


##### Fixes

* Edit changelog settings to maintain older versions changes in change
  log. [Eric Blum]

v2.1.1 (2017-08-10)
-------------------

### Client Facing


##### Fixes

* Removed having multiple tooltips on the notification name in the
  notification feed. [Brian Maw]

* Aligning temperature and energy data by date so that there are no
  zooming issue. [Brian Maw]

* Fixed “Show All” zooming issue that’d cause misaligned charts
  (temperature and main chart). [Brian Maw]


### Internal


##### Fixes

* Fix bug relating to escalating notification logs with the
  SeverityEscalationMixIn. [Eric Blum]

v2.1.0 (2017-08-09)
-------------------

### Client Facing


##### New Features

* Added option to show temperature as a secondary chart on any data
  chart. Will default to open for HVAC equipment and weather based
  loads. [Eric Blum]

* Created Baseline Notification email template. [Eric Blum]

* Pair Or Notification and Baseline Notification implementations for
  active testing. [Eric Blum]

    Pair Or Notification

    * Targets: Equipment with Pair (or) tag

    * Trigger: Combined usage below .1 kW for 30 minutes on a 15-minute interval level

    Baseline Notification

    * Targets: HVAC equipment with mean usage above .1 kW and low variability (consistent usage)

    * Trigger: Usage below inferred baseline (1/3rd of lowest value over 30 days) for over 30 minutes on a 15-minute interval



##### Changes

* Update baseline notification name to include equipment name. [Eric
  Blum]


##### Fixes

* Only consider current data (past 24 hours) when evaluating whether a
  baseline has been passed for the Baseline Notification. [Eric Blum]


### Internal


##### New Features

* Tracking machine.start_date. [Felix]

    - refactored equipment/machine.long_name
    - updated sync
    - added column machine.start_date
    - updated machine admin



##### Changes

* Update README-NOTIFICATION.md to give more examples on how to create
  notification engines. [Eric Blum]

* Update README with development/release step by step guide and more
  info on branching and releasing. [Eric Blum]

* Implement NotificationEngine testing. NotificationLogs have a test
  flag that will disallow it from being shown in platform but still be
  available for processing. [Eric Blum]

* Split user client inline form in admin to tenant and non-tenant. [Eric
  Blum]

* Update README with changelog tutorial and improve changelog generation
  logic. [Eric Blum]

* Modify references of notification_log_poly to notification_log. [Eric
  Blum]


##### Fixes

* Fixed get_sensor_equipments query for report. [Felix]

* Workaround for bitbucket markdown indentation. [Eric Blum]

* Dont check for latest commit on updating changelog with version_bump.
  [Eric Blum]

v2.0.6 (2017-08-07)
-------------------

### Client Facing


##### Fixes

* Dev fixed sensor_equipments location query. [Felix]

v2.0.5 (2017-08-04)
-------------------

### Client Facing


##### Fixes

* Fixed logic for the deltas on Cost and Consumption banner tiles on the
  portfolio view. [Brian Maw]

v2.0.4 (2017-08-04)
-------------------

### Client Facing


##### Fixes

* Fixed duplicate numbers on “Percent Change” banner tile on location
  and portfolio views. [Brian Maw]


### Internal


##### Fixes

* Fixed a parse:syntax with Angular that related to the duplication of
  numbers on the banner. [Brian Maw]

v2.0.3 (2017-08-04)
-------------------

### Client Facing


##### Fixes

* Fixed duplication of numbers on the “Percent Change” banner tile for
  location and portfolio view. [Brian Maw]


### Internal


##### Changes

* Add option to only update changelog in version_bump. [Eric Blum]

* Add gitchangelog and pystache to requirements.txt. [Eric Blum]

v2.0.2 (2017-08-04)
-------------------

### Internal


##### Changes

* Automatically update change log on usage of `version_bump.sh` [Eric
  Blum]

* Change chg tag to Change in changelog. [Eric Blum]

* Add changelog template file. [Eric Blum]


##### Fixes

* Fix .gichangelog.rc settings. [Eric Blum]

    Remove version bump commit from changelog and
    correct tag filtering


